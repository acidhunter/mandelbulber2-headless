# About
Dockerfile to get up a [mandelbulber2](https://github.com/buddhi1980/mandelbulber2) render node in headless mode

Dockerized mandelbulber2 server in gui mode can be found [here](https://github.com/acidhunter/dockercask)

# Setup a rendernode from latest stable release
```bash
docker run -it -e IP=your_mandelbulber2_server -e PORT=5555 acidhunter/mandelbulber2-headless:2.11  
```

# Setup a rendernode from git master
```bash
docker run -it -e IP=your_mandelbulber2_server -e PORT=5555 acidhunter/mandelbulber2-headless:developer
```



## License
Copyright (c) 2017 Martin Friedrich @acidhunter.
Released under the MIT license. See [license](LICENSE.MD)

## Our art project
[geister.mit.zeit](https://www.facebook.com/geister.mit.zeit/)

## Contact
- [Telegram](https://t.me/acidhunter)
- [Twitter](https://twitter.com/Panic_Network)
- [Facebook](https://facebook.com/geist.mit.zeit)
- [Google+](https://plus.google.com/+MartinF)


